+++
date = "2017-06-18T21:39:53Z"
title = "About"

+++

This is my blog.

Let's see how it goes.

## License

Use this site's content for whatever use.
It is licensed under [Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/).

## Some profiles of mine

* [Email](mailto:c-h-r-i-s@barry.im?subject=remove%20the%20dashes)
* <a rel="me" href="https://github.com/chris-barry">Github</a>
* <a rel="me" href="https://chaos.social/@chris">Mastodon</a>
* [Wikipedia](https://en.wikipedia.org/wiki/Special:Contributions/Acebarry)

