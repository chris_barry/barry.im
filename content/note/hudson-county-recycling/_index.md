+++
date = "2017-06-18T21:56:02Z"
title = "Hudson County Recycling"

+++

## Plastic Bags

### Hoboken

#### ShopRite

- Where: <https://www.openstreetmap.org/way/27081357>
- Site: <https://shop.shoprite.com/store/7EF2370>
- Hours: Mo-Sa 07:00-00:00; Su 07:00-24:00

## Composting

### Hoboken

#### City

- Where: <https://www.openstreetmap.org/way/400864014>
- Site: https://www.hobokennj.gov/resources/compost
- Hours: Mo 08:00-12:00; Sa 09:00-12:00

### Jersey City

#### River View Farmers Market

- Where: <https://www.openstreetmap.org/relation/7766498>
- Site: <http://www.riverviewfarmersmarket.org/>
- Hours: May-Nov Su 10:00-15:00

