+++
date = "2019-01-03T21:56:02Z"
title = "recipes"

+++

## Pasta
- [Mac & Cheese](https://www.simplyorganic.com/community/recipe/homemade-vegan-mac-and-cheese-with-nutritional-yeast)

## Soy
- [Tofu](http://www.marystestkitchen.com/diy-tofu-just-soymilk-lemon-water/)
- [Okara Nuggets](https://raggamuslims.wordpress.com/2008/09/26/okara-nuggets/)
- [Veggie groud beef](https://thestingyvegan.com/vegan-taco-stuffed-tomatoes/)
	- Ignore the tomato stuff, ignore taco season, use extra tomato sauce and msg

## Milk
- [Soy Milk](http://www.marystestkitchen.com/diy-soy-milk-recipe/)
- [Oat Milk](https://minimalistbaker.com/make-oat-milk/)

## Soup
- [Tomato Soup](https://avirtualvegan.com/instant-pot-tomato-soup/)
- [Pumpkin Soup](https://theveganstoner.blogspot.com/2015/11/pumpkin-soup.html)
- [Pea soup](https://healthiersteps.com/recipe/instant-pot-vegan-split-pea-soup-recipe/)

## Breakfast
- [French Toast](https://itslivb.com/2019/03/13/the-best-vegan-french-toast/)

## Dessert
- [Sugar Cookies](https://minimalistbaker.com/1-bowl-vegan-sugar-cookies/)

## Misc
- [Parmesan](https://minimalistbaker.com/how-to-make-vegan-parmesan-cheese/)
- [Ranch](https://tasty.co/recipe/vegan-ranch-dip)
