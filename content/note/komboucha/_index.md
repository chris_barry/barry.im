+++
date = "2019-01-03T21:56:02Z"
title = "Komboucha"

+++

Started    | Ended      | Batch | Flavors                                      | Status
-----------|------------|-------|----------------------------------------------|-------
2018-12-12 | 2019-12-19 | 1     | 2 x plain                                    | I  liked it but I did not do a second fermantation
2018-12-19 | 2019-12-28 | 1     | 2 x mango flavored                           | Came out okay
2018-12-28 | 2019-01-04 | 1     | 4 x kiwi,                                    | came out good
2019-01-04 | 2019-01-11 | 1     | 2 x green apple; 2 x kiwi                    | came out good
2019-01-11 | 2019-01-20 | 1     | 3 x green apple; 2 x strawberry; 1 x banana  | Made like 500ml more than normal and was traveling so f1 lasted longer than normal
2019-01-18 | 2019-01-25 | 2     | 2 x red apple; 2 x kiwi; 2 x pineapple       | Took top layer of scobe from other batch and started a second ferment
2019-01-20 | 2019-01-27 | 1     | 2 x pineapple; 1 x brown sugar; 1 x garlic   | Trying some strange stuff
2019-01-25 | 2019-02-02 | 2     | 6 x green apple                              | n/a
2019-01-27 | 2019-02-06 | 1     | 2 x brown sugar; 1 x date; 2 x pineapple     |
2019-02-02 | 2019-02-09 | 2     | 6 x green apple                              |
2019-02-06 | 2019-02-18 | 1     | 3 x green apple                              |
2019-02-09 | 2019-02-18 | 1     | 3 x green apple                              |
2019-02-18 | 2019-03-01 | 1     | 5 x green apple                              |
2019-02-18 | 2019-03-01 | 2     | 5 x green apple                              |
2019-03-01 | 2019-03-15 | 1     | 5 x green apple                              |
2019-03-01 | 2019-03-15 | 2     | 5 x green apple                              |
2019-03-15 | 2019-03-24 | 1     | 4 x green apple                              |
2019-03-15 | 2019-03-24 | 2     | 1 x green apple; 1 x cardimum                |
2019-03-24 | 2019-04-09 | 1     | 5 x ginger                                   |
2019-03-24 | 2019-04-09 | 2     | 5 x green apple                              |
2019-04-10 | 2019-04-23 | 1     | 6 x green apple                              |
2019-04-10 | 2019-04-23 | 2     | 5 x green apple                              |
2019-04-24 | 2019-06-10 | 1     | 5 x red apple ; 1 x kiwi + apple             |
2019-04-24 | 2019-06-10 | 2     | 5 x kiwi      ; 1 x kiwi + apple             |
2019-06-10 | 2019-0x-xx | 1     | ? x green apple                              |
2019-06-10 | 2019-0x-xx | 2     | ? x green apple                              |
