+++
date = "2018-04-15T00:00:00Z"
title = "Jenkins"

+++

Some useful commands you can run from the Jenkins script console.

## Printing Plugin Versions

	println "Running plugin enumerator"
	println ""
	def plugins = jenkins.model.Jenkins.instance.getPluginManager().getPlugins()
	plugins.each {println "${it.getShortName()}:${it.getVersion()}"}
	println ""

Source: <http://stackoverflow.com/a/9822818>

## Running Shell Command in 

	println new ProcessBuilder('sh','-c','ls').redirectErrorStream(true).start().text

Source: <http://jenkins-ci.361315.n4.nabble.com/shell-commands-scripts-in-Script-Console-td2717925.html>
