+++
date = "2018-04-15T00:00:00Z"
title = "OpenStreetMap"

+++

Some queries that can be used 

## Parks
- Parks in Hudson County: <https://overpass-turbo.eu/s/xVb>

## Bikes
- Bicycle parking in Hudson County: <https://overpass-turbo.eu/s/xVc>
- Bicycle parking without capacity: <https://overpass-turbo.eu/s/xVq>✝
- Bicycle parking without type of parking: <https://overpass-turbo.eu/s/xVr>✝
- Bicycle parking without type of access: <https://overpass-turbo.eu/s/xVx>✝

### Sources

- [Jersey City Bike Map](http://data.jerseycitynj.gov/dataset/bikelanes-polygon/resource/9ad98fab-07f3-480c-9f31-b7a633a2e0f7)
- [Hoboken Bike Map](http://hobokennj.gov/departments/transportation-parking/bicycling/)

## Internet Access
- Internet access areas: <https://overpass-turbo.eu/s/xVf>
- Free wifi that should be `internet_access=*`: <https://overpass-turbo.eu/s/xVe>✝
- Internet access, but fee not listed: <https://overpass-turbo.eu/s/xVg>✝
- Internet access, but non-standard tag: <https://overpass-turbo.eu/s/xVi>✝

✝: should be empty
