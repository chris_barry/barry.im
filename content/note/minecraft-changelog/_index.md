+++
date = "2019-01-03T21:56:02Z"
title = "Minecraft Changelog"

+++

Minecraft Changelog

Change   | Date                | What
---------|---------------------|------------------------
CHG00001 | 2019-05-08@01:00UTC | Minecraft 1.13 -> 1.14
CHG00002 | 2019-05-22@02:00UTC | Minecraft 1.14 -> 1.14.1 |
