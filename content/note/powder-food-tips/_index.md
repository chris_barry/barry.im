+++
date = "2017-06-18T21:56:02Z"
title = "Powder Food Tips"

+++

<small>2017-07-23</small>

An Internet friend of mine was asking about tips to make Powdered Food taste better.
I wrote down my recipes.

Any of these recipes can be extended with 100mg of caffiene and 200mg of l-yheanine (both availble on the rain forest shopping site).
Tested with Powdered Food 1.8+

* Chocolate
	* 2 scoop food
	* 1 serving stevia
	* 1 tsp coca powder

* Matcha
	* 2 scoop food
	* 1 serving stevia
	* 1 tsp matcha powder

* Coffee
	* 2 scoop food
	* 1 serving stevia
	* 1 tbsp instant coffee

* Vanilla "cookie dough"
	* 2 scoop food
	* 1 serving stevia
	* 1/8 tsp vanilla extract
