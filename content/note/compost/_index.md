+++
date = "2019-01-03T21:56:02Z"
title = "Fantasy Compost"

+++

## Previous Years
- [2019](./2019/)

## 2020

Date       | Weight (lbs) | Weight (kg) | Where
-----------|--------------|-------------|------
2020-01-13 | 5.0          | 2.27        | [Chelsea, NYC][chelsea]
2020-01-15 | 7.5          | 3.40        | [Chelsea, NYC][chelsea]
2020-01-27 | 4.4          | 2.0         | [Chelsea, NYC][chelsea]
2020-01-29 | 3.0          | 1.36        | [Chelsea, NYC][chelsea]
2020-02-11 | 6.0          | 2.72        | [Chelsea, NYC][chelsea]
2020-03-02 | 3.0          | 1.36        | [Chelsea, NYC][chelsea]
2020-03-04 | 6.0          | 2.72        | [Chelsea, NYC][chelsea]
Total      |              |             |

<script src="./script.js"></script>

[chelsea]: https://www.openstreetmap.org/#map=18/40.75599/-74.00605
