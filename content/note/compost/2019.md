+++
date = "2019-12-15T00:00:00Z"
title = "2019 Compost Log"

+++

2019 Compost Checkins

Date       | Weight (lbs) | Weight (kg) | Where
-----------|--------------|-------------|------
2019-01-03 | 17.8         |  8.07        | [Chelsea, NYC][chelsea]
2019-01-09 |  7.7         |  3.49        | [Chelsea, NYC][chelsea]
2019-01-14 |  6.9         |  3.13        | [Chelsea, NYC][chelsea]
2019-01-25 |  7.8         |  3.53        | [Chelsea, NYC][chelsea]
2019-01-29 |  7.1         |  3.22        | [Chelsea, NYC][chelsea]
2019-02-07 |  6.3         |  2.86        | [Chelsea, NYC][chelsea]
2019-02-19 | 10.0         |  4.54        | [Chelsea, NYC][chelsea]
2019-03-24 | 16.0         |  7.26        | [Chelsea, NYC][chelsea]
2019-03-25 | 13.1         |  5.94        | [Chelsea, NYC][chelsea]
2019-04-12 | 17.9         |  8.11        | [Chelsea, NYC][chelsea]
2019-04-12 |  9.9         |  4.49        | [Chelsea, NYC][chelsea]
2019-05-17 | 12.9         |  5.85        | [Chelsea, NYC][chelsea]
2019-06-03 | 14.8         |  6.71        | [Chelsea, NYC][chelsea]
2019-06-25 | 15.0         |  6.80        | [Chelsea, NYC][chelsea]
2019-07-17 | 12.0         |  5.44        | [Chelsea, NYC][chelsea]
2019-08-12 | 20.5         |  9.30        | [Chelsea, NYC][chelsea]
2019-08-19 |  2.0         |  0.90        | [Chelsea, NYC][chelsea]
2019-09-03 |  5.25        |  2.38        | [Chelsea, NYC][chelsea]
2019-10-11 |  6.10        |  2.77        | [Chelsea, NYC][chelsea]
2019-10-29 | 12.0         |  5.44        | [Chelsea, NYC][chelsea]
2019-11-17 |  9.2         |  4.17        | [Chelsea, NYC][chelsea]
2019-12-08 | 22.0         | 10.00        | [Chelsea, NYC][chelsea]
2019-12-14 |  3.0         | 1.36         | [Chelsea, NYC][chelsea]
Total      |              |              |

<script src="./script.js"></script>

[chelsea]: https://www.openstreetmap.org/#map=18/40.75599/-74.00605
