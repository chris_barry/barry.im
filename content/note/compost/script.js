var elements = document.querySelectorAll("body > table")[0]['childNodes'][1]['children'],
	lbs_total = 0,
	kgs_total = 0;

// Loop through the table and sum up each column.
// When we hit an empty node fill it, only the end should be empty
Array.prototype.forEach.call(elements, function(el, i){
	var a = parseFloat(el.childNodes[1].innerText);
	if(a) {
		lbs_total += a;
	} else {
		el.childNodes[1].innerText = lbs_total;
	}

	var b = parseFloat(el.childNodes[2].innerText);
	if(b) {
		kgs_total += b;
	} else {
		el.childNodes[2].innerText = kgs_total;
	}
});
