+++
date = "2017-08-11"
title = "Multiple Social Media Shares Pointing to One URL"

+++

<small>2017-08-xx</small>

Recently at work I was asked to have two different icons show up when you are a URL to prominent social media sites.
The two main social media sites that implement sharing do not account for this.
So you have to do the following.

## Set Up

1. Determine the main URL you'd like to share. Let's say it's https://example.com/cool-stuff .
2. You'll have to set up two a "proxy" pages
3. Determine all the details of what you wanna share.

## The Proxy Page
