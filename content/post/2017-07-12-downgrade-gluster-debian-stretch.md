+++
title = "Downgrade Gluster Versions on Debian"
date = "2017-07-12"
categories = ["technology"]
tags = ["debian"]

+++

Since many companies use older versions of [Gluster][gluster], here's how you use an older version of Gluster client on Debian.

Here's an example of using Gluster 3.6.9 on Debian Stretch, with the [Gluster repositories][gluster-repos].

	$ DEBID=$(grep 'VERSION_ID=' /etc/os-release | cut -d '=' -f 2 | tr -d '"') && \
	$ DEBVER=$(grep 'VERSION=' /etc/os-release | grep -Eo '[a-z]+') && \
	$ echo deb https://download.gluster.org/pub/gluster/glusterfs/3.6/3.6.9/Debian/${DEBID}/apt ${DEBVER} main > /etc/apt/sources.list.d/gluster.list
	$ apt-get update && apt-get install glusterfs-client=3.6.9-1 glusterfs-common=3.6.9-1

The non-obvious part is that you have to include `glusterfs-common` because `glusterfs-client` is actually an alias to it.
Hopefully my full day of smacking my head in to my desk can help someone else.

You might also have to install an older version of libreadline6, but I'll leave that to you to figure out.

Here's some more [documentation][gluster-install-modern] on installing a more modern version of Gluster.

[gluster]: https://www.gluster.org/
[gluster-repos]: https://download.gluster.org/pub/gluster/glusterfs/
[gluster-install-modern]: https://gluster.readthedocs.io/en/latest/Install-Guide/Install/

