+++
title = "Benny and the Feds"
date = "2017-07-30"
categories = ["politics"]
tags = ["new jersey", "foia"]

+++

## Disclaimer

1. This is a work in progress
2. I'm not a trained investigator or anything.
   I'm just a nerd with a computer.
3. This is likely incomplete.
4. Even knowing what I wrote down, I'd eat at Benny's.
5. I cite everything as a link that I can.

## Before we Begin

For those of you who don't know, there is a famous pizzeria in Hoboken, New Jersey named "Benny Tudinos", a.k.a. Benny's.
Many of the resident don't know that in the 1980's the owner of this restaurant was in a precursor of the FBI's [Pizza Connection Trials][pizza-connection].
This trial was about using pizzarias as fronts for laundering cash and distributing drugs.

One day, I was reading [this news story][the-safe-comment], and a somewhat crazy comment claiming that Benny (the owner of Benny's) was involved with the mob.
How could this be?
Benny that nicer older guy from the pizzaria?
Since the accusations in this comment were pretty serious, I had to confirm the accusations.
This is the result of my hunting.

As a companion to this read, I suggest you open [Benny and the Jets][jets] in another tab while reading this.

## March 1985 -- The Trial

On [13 March 1985][trial], "Bari Drishti" (I'll call him Benny, since everyone knows him as that) was arrested with charges of 20 years along with two other men for trafficking heroin thru Benny's.
Along with the charges, one of his co-defendants was convicted of plotting to assassinate the United States Attorney for the Southern District of New York, a.k.a. [Rudy Giuliani][rudy] and the trial judge, [Vincent L. Broderick][vincent-l-broderick].

I can't find much more information on the actual trial since I don't have access to PACER.
Archive.org does host other [federal cases hosted by Judge Broderick][vincent-l-broderick-cases].
I have a future work section at the bottom of this page for more places one could look for more information.

## The Prison Years -- 1985--1995

In 1985 Benny entered prison as inmate 07145-054.
He hopped around prison locations, Lawisburg PA, El Reno, OK, Tuscon AZ, Lompoc, CA, ...
Most the documents I obtained from the FBI make it seem Benny spent most his time in Lompoc.

Benny was released in October 1996.
And just a coincidental side-note, the judge that locked him up died of cancer in March 1995.

## The Release -- 1995--2007

I don't know much of Benny's life after his release.
Seemingly he got his life back on track, and continued to run the pizzeria until.

## The Missing Safe -- 2007

A [safe went missing][the-safe] with a lot of cash 🤔💭.
The safe weighed 298lbs, and was carried away by four men.

I am not sure how a safe is stolen from a basement in Hoboken without any witnesses, but it happened.
I only have conspiracy theories on this event.

## The End -- 2015

10 October 1937 -- 3 September 2015

## The FOIA

One day after Benny's death (4 September 2015), I filed a [Freedom Of Information Act (FOIA) request to the FBI][foia] for their case files.
It wasn't until 31 March 2016 that I started to receive responses!
The responses continued until 19 December 2016.
I have also archived the results of the FOIA on [archive.org][archive].

Some fun facts on the FOIA:

* The FBI Agent assigned to my case was able to misspell my last name Barry as Berry
* If I didn't know better, I would have thought the FBI was advertising Benny Tudinos.
  The phrase, "Drishti ran Benny Tudinos, one of the top ten pizzarias in NJ, ..." is used many times.
* Filing FOIA's is easy, but the waiting is the hard part.
* FBI Agents have crap handwriting
* FBI buys the shittiest photocopiers 
* Benny's SSN is in it 131-42-5382

The following sections will highlight some interesting events, along with the name of the file on MuckRock.

### `1336476-0_-_89B-NY-785_Section_1.PDF`

* Benny denied all charges, even to the point of tears - page 15
* "NO DOUBT IN HIS MIND [THEY'RE] GOING TO KILL HIM" - page 26
* Letters between Benny and his daughter or wife (it's redacted but I'm using context clues) - 28--46
	* It seems like an agent was paraphrasing the letters
* Benny's family had to sell their Hoboken house for bail money - page 44
* The Hoboken community apparently did not believe him to be guilty - page 44
* He was born in Shkodra, Albania - page 66
* The paraphrased letters are here! - page 76
* Benny fought off state charges twice before the feds won - page 92

### `1336476-0_-_89B-NK-63569_Section_1.PDF`

* Benny takes a polygraph examination in 1989 - page 30

### `1336476-0_-_89-HQ-5884_Section_1.PDF`

* "These individuals were central figures in a 'loose' group of Albanian-Yugoslavian drug dealers responsible for importing in excess of 50 kilos of heroin.
   This group operated between the years 1978 and 1984 and is allegedly responsible for numerous murders and attempted murders" - page 20
* Sounds like Benny tried to give information for a better jail sentence? All the juicy stuff is redacted - page 25 
* Benny cooperated with FBI Agents in the summer of 1984 with respect to communist activity of Albanians in the United Nations - page 28
* Updates were sent to the then director of the FBI (William H. Webster) directly - page 42

### `1336476-0_-_89-HQ-5884_Unrecorded_Serial.PDF`

* Rudy Guliani was personally notified of the assassination plot. - page 2

### `1336476-0_-_89BNY-785_Albanian_Letters_Section_2.PDF`

Mainly translated letters Benny sent/received while incarcerated.
These were in other documents, but they're more clear here.

* Some funny doodles - page 26

### `12-14-16_MR21148_RES.PDF`

Transcriptions of translated telephone conversations.
Mostly chit chat.

## Future Work

* I found these on an academic proxy I used to have access to.
	* 1997 U.S. Dist. LEXIS 20674,
	* <http://ezproxy.stevens.edu:2094/lnacui2api/api/version1/getDocCui?lni=3RR0-0DF0-0038-Y0GK&csi=6323&hl=t&hv=t&hnsd=f&hns=t&hgn=t&oc=00240&perma=true>
	* 2002 U.S. Dist. LEXIS 23917,
	* <http://ezproxy.stevens.edu:2094/lnacui2api/api/version1/getDocCui?lni=47FX-80V0-0038-Y177&csi=6323&hl=t&hv=t&hnsd=f&hns=t&hgn=t&oc=00240&perma=true>
	* 2003 U.S. App. LEXIS 19075,
	* <http://ezproxy.stevens.edu:2094/lnacui2api/api/version1/getDocCui?lni=49JB-88J0-0038-X23M&csi=6320&hl=t&hv=t&hnsd=f&hns=t&hgn=t&oc=00240&perma=true>
* There might be more documents here.
	* <https://ecf.nysd.uscourts.gov/cgi-bin/login.pl>
* <https://www.bop.gov/inmateloc/>
	* 07145-054

[jets]: https://www.youtube.com/watch?v=p5rQHoaQpTw
[pizza-connection]: https://en.wikipedia.org/wiki/Pizza_Connection_Trial
[trial]: https://www.nytimes.com/1985/03/14/nyregion/the-city-head-of-drug-ring-gets-life-in-prison.html
[rudy]: https://en.wikipedia.org/wiki/Rudy_Giuliani
[vincent-l-broderick]: https://en.wikipedia.org/wiki/Vincent_L._Broderick
[vincent-l-broderick-cases]: https://archive.org/search.php?query=collection%3A%28usfederalcourts%29%20AND%20assigned-to%3A%28Judge%20Vincent%20L.%20Broderick%29
[the-safe]: http://hoboken411.com/archives/5051
[the-safe-comment]: http://hoboken411.com/archives/5051#wc-comm-184754_184749
[foia]: https://www.muckrock.com/foi/united-states-of-america-10/bari-drishti-case-files-21148/
[lampoc]: https://en.wikipedia.org/wiki/United_States_Penitentiary,_Lompoc
[archive]: https://archive.org/details/Benny-Tudino-FBI-Files
