+++
title = "Vanilla Minecraft Logrotate"
date = "2020-01-01"
categories = ["technology"]
tags = ["minecraft"]

+++

For various reasons, I wanted to use the standard `logrotate` that comes with Debian along with a Vanilla Minecraft server.
There are a lot of other Minecraft servers, but I'm going to show how to set this up with Vanilla 1.15.1.
This set up should work with other versions too, but I didn't test it.
Thankfully Minecraft ships with a popular Java logging framework, [log4j](https://en.wikipedia.org/wiki/Log4j), so this was pretty easy to sort out.

I'm going to assume your server files are in `/opt/minecraft`.

## /opt/minecraft/log4j.xml
This file tells log4j how to write logs.
I am overwiring the default `RollingRandomAccessFile` with a basic `File` appender.

```
<?xml version="1.0" encoding="UTF-8"?>
<!-- Forked from default log4j.xml from server-1.15.1.jar -->
<Configuration status="WARN" packages="com.mojang.util">
	<Appenders>
		<Console name="SysOut" target="SYSTEM_OUT">
			<PatternLayout pattern="[%d{HH:mm:ss}] [%t/%level]: %msg%n" />
		</Console>
		<Queue name="ServerGuiConsole">
			<PatternLayout pattern="[%d{HH:mm:ss} %level]: %msg%n" />
		</Queue>
		<!-- Use a basic file log -->
		<File name="File" fileName="logs/latest.log">
			<PatternLayout pattern="[%d{HH:mm:ss}] [%t/%level]: %msg%n" />
		</File>
	</Appenders>
	<Loggers>
		<Root level="info">
			<filters>
				<MarkerFilter marker="NETWORK_PACKETS" onMatch="DENY" onMismatch="NEUTRAL" />
			</filters>
			<AppenderRef ref="SysOut"/>
			<AppenderRef ref="File"/>
			<AppenderRef ref="ServerGuiConsole"/>
		</Root>
	</Loggers>
</Configuration>

```

## /etc/logrotate.d/minecraft
This tells logrotete how to roll log files.
It is configured to keep seven days of logs.
Most operating systems will run this at midnight of your time zone.

The output files will be owned by the minecraft user, and will be gzipped.
The [logrotate man page](https://linux.die.net/man/8/logrotate) has more information on how to configure this.

```
/opt/minecraft/data/logs/latest.log {
 rotate 7
 daily
 compress
 missingok
 copytruncate
 create 640 minecraft minecraft
}
```

## Running Params

`java -Xmx4096M -Xms4096M -jar server.jar -Dlog4j.configurationFile=/opt/minecraft/log4j.xml nogui`
