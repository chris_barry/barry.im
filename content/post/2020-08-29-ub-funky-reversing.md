+++
title = "Reverse Engineering U.B. Funkeys"
date = "2020-08-29"
categories = ["technology"]
tags = ["ub-funky", "reverse-engineering"]

+++

U.B. Funkeys is a toys-to-live game that was released in 2007.
It's quite similar to Nintendo Amiibos, but pre-dates it by a few years and uses much different hardware.
Despite being released 13 years ago, there is still an active fandom, discussion forums, and even mods.
But somehow there is no information on the hardware.

## Hardware

My significant other has quite a collection of Funkeys, with some broken ones.
I was able to disassemble a Funkey (figure 1) and a Funkey Hub (figure 2), the two main pieces required to run the game.

<figure>
    <img src="/2020-08-29-ub-funky-reversing/funkey-front.jpg" decoding="async" loading="lazy" alt="A red Funkey on top of measuring paper. It is a red humanoid form." />
    <figcaption>Figure 1: An example of a Funkey.</figcaption>
</figure>
<figure>
    <img src="/2020-08-29-ub-funky-reversing/funkey-hub.jpg" decoding="async" loading="lazy" alt="The hub looks just like a funkey, but it much larger. Additionally a USB A cable comes out the back like a tail." />
    <figcaption>Figure 2: The Funkey reader.</figcaption>
</figure>

### The Funkey

This is the more simple of the two components.
The Funkey is used in the game to switch characters in the game.

The funky's base is really made up of four resistors and five contact points (figure 3).
I was unable to find any obvious relationship between the resistor values and the resistance values I could read from the pad.
These pads connect to the reader in the hub which houses a little magnet so that it stays connected (figure 4)

<figure>
    <img src="/2020-08-29-ub-funky-reversing/funkey-thing-top.jpg" decoding="async" loading="lazy" alt="A PCB board taken from the normal sized Funkey. It's black, rectangular, and has surface mount resistors." />
    <figcaption>Figure 3: A board extracted from the feet of a little Funkey.</figcaption>
</figure>
<figure>
    <img src="/2020-08-29-ub-funky-reversing/funkey-thing-bottom.jpg" decoding="async" loading="lazy" alt="The bottom side of the PCB. It has 5 golden pads that are seperated by a horizontal line." />
    <figcaption>Figure 4: The bottom side of the same board.</figcaption>
</figure>

### Reader

This is where most of the logic in this system exists.
The figure is a much larger funky with a reader in the back of it's head, which is then plugged in to a computer using USB.
I was unable to find any documents showing the internals from the FCC or pictures of the hub torn down on search engines.
I was not so successful with reversing any of the hardware, but here's what I found anyway!

The top (figure 5) mainly contained the input USB wiring (which was not soldered well at all), the pads that are used to read the funky, and some test pads.
The test pads on the top were really interesting to me since the four on the top (P61, P62, P63, P65) has the same number of inputs as uart.
By probing around with a multimeter I found it unlikely that they are used for uart.
Three of four pads output a constant 3 Volt signal.

<figure>
    <img src="/2020-08-29-ub-funky-reversing/funkey-hub-inside-top.jpg" decoding="async" loading="lazy" alt="The Funkey reader taken apart. The inside of the Funkey is plastic, and connected via four wires is another PCB. The PCB has five levers attached that look like metal fingers." />
    <figcaption>Figure 5: The top of the PCB that drives the Funkey reader.</figcaption>
</figure>
<figure>
    <img src="/2020-08-29-ub-funky-reversing/funkey-hub-inside-bottom.jpg" decoding="async" loading="lazy" alt="The opposite side of the PCB/ It's covered in surface mounted components." />
    <figcaption>Figure 6: The bottom side of the PCB that drives the Funkey reader.</figcaption>
</figure>

The bottom (figure 6) contained an array of capacitors, resistors, and an epoxy blob.
Notably there were no <abbr title="Integrated Circuit">ICs</abbr> that I could search for more information on the board.
Since there is a pandemic happening I figured I should not invest in potentially dangerous chemicals to remove the epoxy.
That's a project for another day.

## Software

Plugged in the unit and ran `lsusb` to find the following:

    $ lsusb
    ...
    Bus 003 Device 003: ID 0e4c:7288 Radica Games, Ltd funkey reader
    ...

Found ProductID and VendorID via normal `lsusb`
I opened Wireshark to watch the connection and there were a bunch of set-up packets that showed the vendor's name which helped me be sure that I was watching the right stream.
I tried to plug in the Funkey and nothing happened.
Which led me to believe that there was some sort of notice that the Funkey game was sending to the hub to tell it to start transmitting.

But the game only works on Windows XP.
Which means we get to boot up XP!!

### Sniffing the Connection

Since I wasn't able to make any progress with the hardware, I guess I should check out the software.
Maybe it's a little late in to this post to say, but this project was all started because the latest operating system that U.B. Funkey works on is Windows XP.
I have heard some people have came out with mods to run it on more modern versions of Windows but I don't know much about that.

In order to see how this worked I had to set up Windows XP in a <abbr title="Virtual Machine">VM</abbr>.
With the assistance of my significant other I was able to install U.B. Funkey in to the VM.
Once that was working I installed Wireshare on the host machine and I was able to sniff USB traffic!
If you want to follow along yourself the pcap file is available. TODO TODO TODO

There are a whole bunch of set-up packets that are sent that don't contain much important information.
I noticed that two packets were sent in parallel, one control packet and one interrupt packet.

    > computer to hub: URB control
    > computer to hub: URB interrupt
    < hub to computer: `00 00 00 00`
    < hub to computer: `ff ff ff ff ff 00 00`

The control packet seems to alert the hub that the connection is set up and it can begin responding to requests to send information about the Funkey.
The interrupt packet returns a unique id specific to each Funkey with `ff ff ff ff ff 00 00` being used when the reader is empty.
The hub was a little erratic in the values it returned, but after requesting the value a few times it would settle down on a value.
My significant other assured me that the hub was not broken and that when playing the game you'd often have to try to register the Funkey a few times.
 
### Writing a Reader

Since the protocol was so simple, had no obfuscation, and only works on Windows XP it seemed like a good idea to write a simple driver that would set up a connection, request a FunkeyID, and wait.
I uploaded the code to [Gitlab](https://gitlab.com/chris_barry/ub-reader).

To use the reader, you have to install python3, pip, libusb through pip.
It can then be read like this (assuming you're running a \*nix based operating system):

    $ pip3 install libusb
    $ python3 read-from-hub.py

Right now the reader is only able to print out the hex code of the Funkey.
I am sure that someone else could expand it to do something more useful.

