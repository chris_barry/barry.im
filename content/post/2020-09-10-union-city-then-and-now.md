+++
title = "Union City Then and Now"
date = "2020-09-10"
categories = ["history"]
tags = ["new jersey", "union city"]

+++

Recently I found a great book about this history of Union City (then West Hoboken) on [archive.org][archive].
The book, [History of West Hoboken, N.J by Drescher, William H., Jr.][book], contained a lot of photos of West Hoboken in 1903.
Surprisingly I recognized several of the buildings!
And it was even better that the author provided a map of West Hoboken with the old street names, and the corner or street most of the photos were taken at.

I was also able to upload the extracted images to [Wikimedia][wikimedia].
That page has more images than here, this page just has images of the places that are still around.

## How I Extracted the images

I downloaded the JP2 extract from archive.org.
These files are the raw scans.
Next, I went through page by page and opened the JP2 files in Gimp 2.10.18 and corrected the angle to try and straighten out the picture.
Next I exported the picture as a JPG with the default settings.

For the modern the modern pictures I used my Pixel 3, and cropped them using Gimp also.

## The Pictures

### Map

This is a map of West Hoboken from 1903.
This was before all the roads were numbered sequentially.

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-map.jpg" decoding="async" loading="lazy" />

### Cliton Near Monastery

GPS: 40.76520,-74.03245

This block as a whole really looked the same.
Notably there is no more trolley and the entire road is dedicated to private vehicle storage.

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-clinton-near-monastery-and-stevens-export.jpg" decoding="async" loading="lazy" />

### First Presbyterian Church

GPS: 40.75434,-74.03934

This was the first church I went to.
Even though I commuted by this church every day for two years on foot, I didn't even recognize it from the image!

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-first-presbyterian-church-export.jpg" decoding="async" loading="lazy" />

### German Baptist Church

GPS: 40.756983,-74.038639

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-german-baptist-church-export.jpg" decoding="async" loading="lazy" />

### German Methodist Church

GPS: 40.76655/-74.03153

Looks like this church has lost it's steeple, but it's still recognizable!

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-german-methodist-church-export.jpg" decoding="async" loading="lazy" />

### Monastery

GPS: 40.76498/-74.03624

I had a hard time getting a photo at the same angle.
Currently the trees are too tall and block the view.
But trust me, I'd rather the trees than some view.

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-monestary-export.jpg" decoding="async" loading="lazy" />

### Northwest Spring and Dodd

GPS: 40.76818/-74.03207

I walk by the corner somewhat often and had no idea it looks almost the same as from 1903!
That trolley looks great.

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-northwest-spring-and-dodd-export.jpg" decoding="async" loading="lazy" />

### St John's Church

GPS: 40.76136/-74.03337

I think this was the most similar building.
If you look closely at the big window on the face of the building you can see the original flower design is still there.

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-st-johns-church-export.jpg" decoding="async" loading="lazy" />

### St Joseph's Church

GPS: 40.76201/-74.03909

This church's steeple [burned down in 2017][burning].
Thankfully a new steeple was able to be added back and I can hear the church bells every day.

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-st-josephs-church-export.jpg" decoding="async" loading="lazy" />

### St. Matthew's Church

GPS: 40.76583/-74.03202

I wasn't sure if I should include this one because it's not the same building.
However it is still Saint Matthew's Church in the same location.

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-st-matthews-church-export.jpg" decoding="async" loading="lazy" />

### St Michael's Lyceum

GPS: 40.76108/-74.03493

Again, this is a building I pass all the time and I had no idea of it's age.

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-st-michaels-lyceum-export.jpg" decoding="async" loading="lazy" />

### Swiss Church

GPS: 40.76913/-74.03149

This church was sneaky to find.
The book I was referencing only said the street name, not the corner like the rest said.
Aside from the expanded entrance it looks really similar to the original design.

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-swiss-church-export.jpg" decoding="async" loading="lazy" />

### United Presbyterian Church

GPS: 40.76615/-74.03027

This church confused me.
First I noticed that the buildings on either side are still around.
The next thing I noticed was that the church now has two stories?
I am not sure if they rebuilt the church or if they raised it.

<img src="/2020-09-10-union-city-then-and-now/west-hoboken-1903-united-presbyterian-church-export.jpg" decoding="async" loading="lazy" />

## Conclusion

Overall this was a great way to look around my neighborhood and learn a bit about it while being socially distant.
Check out archive.org and see if you can find a history book about the place you live.
If you enjoyed this post, consider donating to the Internet Archive.
This wouldn't have been possible without their amazing service.

It's amazing how much of the built world around me was determined so long ago.
The streets were laid out and the buildings were constructed without any input from the people who live in cities currently.
I wonder how how many of today's decisions will stay impact people in 117 years.

[archive]: https://archive.org
[book]: https://archive.org/details/historyofwesthob00dres
[wikimedia]: https://commons.wikimedia.org/wiki/Category:West_Hoboken,_New_Jersey
[burning]: https://www.youtube.com/watch?v=5s6xyyvyim4
