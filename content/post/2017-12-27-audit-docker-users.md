+++
title = "Audit Docker Users"
date = "2017-12-27"
categories = ["technology"]
tags = ["docker"]
+++

During my winter break I decided it would be worthwhile to audit what docker containers I have running.
In the process of making sure my containers were dropping their user from root to a less privileged user.

This should work for any docker unixy image (e.g. alpine, centos, debian, redhat).

If your container returns root, you should investigate dropping the user to something safer 🐳.

```
docker-users() {
for RUNNING in $(docker ps --format '{{.Names}}:{{.ID}}')
  do
    NAME=$(awk -F: '{print $1}' <<< $RUNNING)
    ID=$(awk -F: '{print $2}' <<< $RUNNING)
    echo $NAME user: $(docker exec -it $ID whoami)
  done
}   
```
