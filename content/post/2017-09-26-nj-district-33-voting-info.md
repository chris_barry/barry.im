+++
title = "New Jersey District 33 Voting Information"
date = "2017-09-26"
categories = ["politics"]
tags = ["new jersey"]

+++

I coundn't find much information for voters in the [33rd District of New Jersey](https://en.wikipedia.org/wiki/33rd_Legislative_District_(New_Jersey)), so I wrote one up,
It's probably not perfect, but you can at least kick start your research from here.

In all honesty I found it difficult to find information on the republican candidates.

## State Senator

### Democrat

* Brian P. Stack - incumbant
	* Wikipedia - <https://en.wikipedia.org/wiki/Brian_P._Stack>
	* [Offical Senator page](http://www.njleg.state.nj.us/members/BIO.asp?Leg=306) - voting record available

### Republican

* Beth Hamburger
	* Ballotpedia - <https://ballotpedia.org/Beth_Hamburger>
	* Likely her LinkedIn - <https://www.linkedin.com/in/beth-hamburger-3223b84>

## State Assembley

### Democrat

* Annette Chaparro - incumbant
	* Wikipedia - <https://en.wikipedia.org/wiki/Annette_Chaparro>
	* [Official Assemblyperson page](http://www.njleg.state.nj.us/members/BIO.asp?Leg=385) - voting record available
* Raj Mukherji - incumbant
	* Wikipedia - <https://en.wikipedia.org/wiki/Raj_Mukherji>
	* [Official Assemblyperson page](http://www.njleg.state.nj.us/members/BIO.asp?Leg=364) - voting record available

### Republican

* Holly Lucyk
	* Ballotpedia - <https://ballotpedia.org/Holly_Lucyk>
	* Likely her LinkedIn - <https://www.linkedin.com/in/holly-lucyk-555924a3>
* No Nomination

## Hudson County Clerk

### Democrat

* E. Junior Maldonado
	* Ballotpedia - <https://ballotpedia.org/E._Junior_Maldonado>

### Republican

* Blake Lichtenberger  
	* Ballotpedia - <https://ballotpedia.org/Blake_Lichtenberger>

## Freeholder

### Democrat

* Tilo Rivas
	* Wikipedia - <https://en.wikipedia.org/wiki/Tilo_Rivas>

### Republican

* No Nomination
